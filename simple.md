##Массивы и итераторы
```
names = ['Ада', 'Белль', 'Крис']
puts names
puts names[0]
puts names[1]
names.first
names.last
```
each

```
languages = ['английский', 'немецкий', 'Ruby']
languages.each do |lang|
 puts 'Мне нравится ' + lang + '!'
end
puts 'А теперь давайте послушаем мнение о C++!'
puts '...'
```

times
```
3.times do
 puts 'Гип-гип-ура!'
end
```

join
```
foods = ['артишок', 'бриошь', 'карамель']
puts foods.join(', ')
```

push, pop и last
```
favorites = []
favorites.push 'капли дождя на розах'
favorites.push 'капли виски на котах'
puts favorites[0]
puts favorites.last
puts favorites.length
puts favorites.pop
puts favorites
puts favorites.length
```